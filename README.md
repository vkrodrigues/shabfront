# Sistema de Sorteio Habitação - Front

Sistema utilizado para executar o sorteio e visualizar os participantes.

## 🚀 Começando

Essas instruções permitirão que você obtenha uma cópia do projeto em operação na sua máquina local.

### 📋 Tecnologias Utilizadas

* [Angular](https://angular.io) - Framework javascript (v13.1.0);
* [Boostrap](https://getbootstrap.com) - Framework javascript (v5.1.3);
* [Javascript](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript) - Liguagem de processamento para web;

### 🛠️ Configurando o Projeto

Utilize o caminho abaixo para clonar o projeto:

```
https://gitlab.com/vkrodrigues/shabfront.git
```

Acesse a pasta do projeto, pela interface do sistema ou com o comando:

```
cd Shab
```

Em seguida instale as dependências com o comando:

```
npm i
```

Iniciando o projeto com o comando:

```
ng serve --open
```

## ✒️ Autores

* **Um desenvolvedor** - *Trabalho Inicial* - [vkrodrigues](https://github.com/Viktoradr)

---
